<?php

use yii\helpers\ArrayHelper;

$aliases = require(STD_PATH_ENGINE . '/Config/common/aliases.php');

$db = require(STD_PATH_ENGINE . '/Config/components/db.php');

$config = [
    'id' => 'install',
    'language' => 'en-US',
    'bootstrap' => ['log'],
    'basePath' => STD_PATH_INSTALL,
    'runtimePath' => STD_PATH_RUNTIME,
    'vendorPath' => STD_PATH_VENDOR,
    'controllerNamespace' => 'Steady\Install\Controllers',
    'aliases' => ArrayHelper::merge($aliases, [
        '@install-frontend' => '@install/Resources/frontend',
    ]),
    'components' => [
        'request' => [
            'cookieValidationKey' => 'JG7KAJDJ29KQKJSJSKKo919WJD',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'install' => 'step/step1',
                'install/step/<action:\d+>' => 'step/step<action>',
            ],
        ],
        'db' => $db,
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'Steady\Engine\Modules\User\Models\UserModel',
            'enableAutoLogin' => true,
            'authTimeout' => 86400,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'formatter' => [
            'sizeFormatBase' => 1000,
        ],
        'i18n' => [
            'translations' => [
                'install' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@install/Resources/messages',
                    'fileMap' => [
                        'install' => 'install.php',
                    ],
                ],
            ],
        ],
        'assetManager' => [
            'basePath' => '@assets',
            'baseUrl' => '@url-assets',
            'forceCopy' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js'],
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [YII_DEBUG ? 'css/bootstrap.css' : 'css/bootstrap.min.css'],
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [YII_DEBUG ? 'js/bootstrap.js' : 'js/bootstrap.min.js'],
                ],
            ],
        ],
    ],
];


if (YII_ENV_DEV) {
    $dev = [
        'bootstrap' => ['gii', 'debug'],
        'components' => [
            'db' => [
                'enableSchemaCache' => false,
            ],
        ],
        'modules' => [
            'gii' => [
                'class' => 'yii\gii\Module',
                'allowedIPs' => ['*'],
            ],
            'debug' => [
                'class' => 'yii\debug\Module',
                'allowedIPs' => ['*'],
            ],
        ],
    ];
    $config = array_merge_recursive($config, $dev);
}

return $config;