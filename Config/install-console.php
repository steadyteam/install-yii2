<?php

$aliases = require(STD_PATH_ENGINE . '/Config/common/aliases.php');

$db = require(STD_PATH_ENGINE . '/Config/components/db.php');

$config = [
    'id' => 'install-console',
    'bootstrap' => ['gii'],
    'basePath' => STD_PATH_INSTALL,
    'runtimePath' => STD_PATH_RUNTIME,
    'controllerNamespace' => 'Steady\Install\Commands',
    'aliases' => $aliases,
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'db' => $db,
    ],
];

return $config;