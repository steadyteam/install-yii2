<?php
?>
<?php $this->beginContent('@app/Resources/views/layouts/main.php'); ?>
<div id="page-install" class="container">
    <div class="row text-center">
        <h1><a href="https://steady-web.com/cms" target="_blank">SteadyWeb</a> installation</h1>
    </div>
    <br/>
    <?= $content ?>
</div>
<?php $this->endContent(); ?>


