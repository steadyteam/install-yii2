<?php

$this->title = 'SteadyWeb installation | Step 2';

?>

<?= $this->render('_steps', ['currentStep' => 2]) ?>

<div class="col-md-6 col-md-offset-3">
    <div class="text-center"><h2>Database</h2></div>
    <br/>
    <div class="well">
        <?= $this->render('forms/_form2', ['model' => $model]) ?>
    </div>
</div>