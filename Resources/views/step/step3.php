<?php

$this->title = 'SteadyWeb installation | Step 3';

?>

<?= $this->render('_steps', ['currentStep' => 3]) ?>

<div class="col-md-6 col-md-offset-3">
    <div class="text-center"><h2>Admin settings</h2></div>
    <br/>
    <div class="well">
        <?= $this->render('forms/_form3', ['model' => $model]) ?>
    </div>
</div>