<?php

use yii\helpers\Url;

$this->title = 'SteadyWeb installation step 1';

?>

<?= $this->render('_steps', ['currentStep' => 1]) ?>

<?php if (false) : ?>
    <h2 class="text-danger">Warning</h2>
    <div class="alert alert-danger">Cannot connect to database. Please configure <code>app/Config/db.php</code></div>
<?php else : ?>
    <div class="row text-center">
        <h2 class="text-muted">If all the requirements are satisfied</h2>
        <a href="<?= Url::to(['/install/step/2']) ?>" class="btn btn-primary btn-lg">
            Continue
            <i class="glyphicon glyphicon-forward"></i>
        </a>
    </div>
    <hr/>
    <?= $this->renderFile(STD_PATH_ROOT . DIRECTORY_SEPARATOR . 'requirements.php') ?>
<?php endif; ?>
