<?php

use Steady\Engine\SW;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<?php $form = ActiveForm::begin(['action' => Url::to(['/install/step/3'])]); ?>

<?= $form->field($model, 'username',
    ['inputOptions' => ['title' => SW::t('install', 'Username to login as admin')]]) ?>
<?= $form->field($model, 'password',
    ['inputOptions' => ['title' => SW::t('install', 'Password to login as admin')]]) ?>
<?= $form->field($model, 'email',
    ['inputOptions' => ['title' => SW::t('install', 'Used as "ReplyTo" in mail messages')]]) ?>
<?= $form->field($model, 'robotEmail',
    ['inputOptions' => ['title' => SW::t('install', 'Used as "From" in mail messages')]]) ?>
<?= $form->field($model, 'recaptchaKey',
    ['inputOptions' => ['title' => SW::t('install', 'Required for using captcha in forms')]]) ?>
<?= $form->field($model, 'recaptchaSecret') ?>

    <p class="recaptcha-tip"><?= SW::t('install', 'You easily can get keys on') ?>
        <a href="https://www.google.com/recaptcha/intro/index.html"
           target="_blank"><?= SW::t('install', 'ReCaptcha website') ?>
        </a>
    </p>

<?= Html::submitButton(SW::t('install', 'Next'), ['class' => 'btn btn-lg btn-primary btn-block']) ?>

<?php ActiveForm::end(); ?>