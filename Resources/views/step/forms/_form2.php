<?php

use Steady\Engine\SW;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<?php $form = ActiveForm::begin(['action' => Url::to(['/install/step/2'])]); ?>
<?= $form->field($model, 'host',
    ['inputOptions' => ['title' => SW::t('install', 'Password to login as root')]]) ?>
<?= $form->field($model, 'database',
    ['inputOptions' => ['title' => SW::t('install', 'Used as "ReplyTo" in mail messages')]]) ?>
<?= $form->field($model, 'username',
    ['inputOptions' => ['title' => SW::t('install', 'Used as "From" in mail messages')]]) ?>
<?= $form->field($model, 'password',
    ['inputOptions' => ['title' => SW::t('install', 'Required for using captcha in forms')]])
    ->passwordInput() ?>
<?= $form->field($model, 'prefix') ?>
    <p class="recaptcha-tip"><?= SW::t('install', 'You easily can get keys on') ?>
        <a href="https://www.google.com/recaptcha/intro/index.html"
           target="_blank"><?= SW::t('install', 'ReCaptcha website') ?>
        </a>
    </p>
<?= Html::submitButton(SW::t('install', 'Next'), ['class' => 'btn btn-lg btn-primary btn-block']) ?>
<?php ActiveForm::end(); ?>