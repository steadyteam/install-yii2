<?php

use Steady\Engine\SW;
use yii\helpers\Url;

$asset = \Steady\Admin\Assets\EmptyAsset::register($this);;

$this->title = SW::t('install', 'Installation completed');
?>
<div class="container">
    <div id="wrapper" class="col-md-6 col-md-offset-3 vertical-align-parent">
        <div class="vertical-align-child">
            <div class="panel">
                <div class="panel-heading text-center">
                    <?= SW::t('install', 'Installation completed') ?>
                </div>
                <div class="panel-body text-center">
                    <a href="<?= Url::to(['/admin']) ?>">Go to control panel</a>
                </div>
            </div>
            <div class="text-center">
                <a class="logo" href="http://steadycms.com" target="_blank" title="SteadyWeb homepage">
                    <img src="<?= $asset->baseUrl ?>/img/logo_20.png">SteadyWeb
                </a>
            </div>
        </div>
    </div>
</div>
