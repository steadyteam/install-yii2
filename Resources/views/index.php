<?php

use Steady\Engine\SW;

$asset = \Steady\Admin\Assets\EmptyAsset::register($this);

$this->title = SW::t('install', 'Installation');
?>
<div class="container">
    <div id="wrapper" class="col-md-6 col-md-offset-3 vertical-align-parent">
        <div class="vertical-align-child">
            <div class="panel">
                <div class="panel-heading text-center">
                    <?= SW::t('install', 'Installation') ?>
                </div>
                <div class="panel-body">
                    <?= $this->render('_form', ['model' => $model]) ?>
                </div>
            </div>
            <div class="text-center">
                <a class="logo" href="http://steady-web.com/cms" target="_blank" title="SteadyWeb homepage">
                    <img src="<?= $asset->baseUrl ?>/img/logo_20.png">SteadyWeb
                </a>
            </div>
        </div>
    </div>
</div>
