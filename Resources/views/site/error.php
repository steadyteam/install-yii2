<?php

use Steady\Engine\SW;

$this->title = SW::t('install', 'Installation error');

?>

<div class="container">
    <div id="wrapper" class="col-md-6 col-md-offset-3">
        <div class="panel">
            <div class="panel-heading text-center">
                <?= SW::t('install', 'Installation error') ?>
            </div>
            <div class="panel-body text-center">
                <?= $message ?>
            </div>
        </div>
        <div class="text-center">
            <a class="logo" href="http://steady-web.com/cms" target="_blank" title="SteadyWeb homepage">
                <img src="<?= $asset->baseUrl ?>/img/logo_20.png">SteadyWeb
            </a>
        </div>
    </div>
</div>
