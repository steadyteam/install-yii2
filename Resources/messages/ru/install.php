<?php

return [
    'Installation' => 'Установка',
    'Install' => 'Установить',
    'Installation completed' => 'Установка завершена',
    'Installation error' => 'Ошибка установки',

    'Next' => 'Далее',

    'Host' => 'Хост',
    'Database name' => 'Имя базы данных',
    'Username' => 'Пользователь',
    'Password' => 'Пароль',
    'Table prefix' => 'Префикс для таблиц',

    'Admin username' => 'Логин администратора',
    'Admin password' => 'Пароль администратора',
    'Admin E-mail' => 'E-mail администратора',
    'Robot E-mail' => 'E-mail рассыльщика',

    'Username to login as admin' => 'Имя пользователя для входа как admin',
    'Password to login as admin' => 'Пароль для входа как admin',
    'Used as "ReplyTo" in mail messages' => 'Адрес "ReplyTo", при отправке писем',
    'Used as "From" in mail messages' => 'Адрес "From", при отправке писем',
    'Required for using captcha in forms' => 'Требуется для использовании каптчи в формах',

    'You easily can get keys on' => 'Вы можете легко получить ключи на',
    'ReCaptcha website' => 'сайте ReCaptcha',

    'Auth time' => 'Время авторизации',
    //'Frontend toolbar position' => 'Позиция панели на сайте',

    'Cannot connect to database. Please configure `{0}`.' => 'Ошибка при подключении к базе данных. Пожалуйста проверьте настройки `{0}`.',
    'SteadyWeb is already installed. If you want to reinstall steadyCMS, please drop all tables with prefix `steady_` from your database manually.' => 'SteadyWeb уже установлена. Если вы хотите произвести переустановку, удалите все таблицы с префиксом `steady_` в вашей базе данных.',
];