<?php

namespace Steady\Install\Services;

use Steady\Admin\Components\SteadyModule;
use Steady\Engine\Handlers\ConfigHandler;
use Steady\Engine\Helpers\WebConsole;
use Steady\Engine\Modules\Module\Models\ModuleModel;
use Steady\Engine\Modules\Setting\Models\SettingModel;
use Steady\Engine\Modules\User\Models\UserModel;
use Steady\Engine\SW;
use Steady\Install\Forms\Step2Form;
use Steady\Install\Forms\Step3Form;
use yii\web\ServerErrorHttpException;

class InstallService
{
    /**
     * @name Step2Form $form
     * @return bool
     * @throws ServerErrorHttpException
     */
    public function step2(Step2Form $form): bool
    {
        $this->createDbConfig($form);

        return true;
    }

    /**
     * @name Step3Form $form
     * @return bool
     * @throws ServerErrorHttpException
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function step3(Step3Form $form): bool
    {
        $this->createAdminConfig($form);

        $webConsole = new WebConsole(SW::getAlias('@install/Config/install-console.php'));
        $migrationResult = $webConsole->migrate();

        $transaction = $transaction = SW::$app->db->beginTransaction();
        try {
            $this->insertSettings($form);
            $this->insertUser($form);
            $this->installModules();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            echo $e->getMessage() . "\r\n";
            echo $e->getTraceAsString();
            exit();
        }

        return $migrationResult;
    }

    /**
     * @name Step2Form $form
     * @return bool
     * @throws ServerErrorHttpException
     */
    private function createDbConfig(Step2Form $form): bool
    {
        $file = STD_FILENAME_DB_PERSONAL_CONFIG;
        $config = ";Generated
            driver = mysql
            host = $form->host
            dbname = $form->database
            username = $form->username
            password = $form->password
            charset = utf8
            tablePrefix = $form->prefix
            enableSchemaCache = true";

        $configHandler = new ConfigHandler();
        $configHandler->createPersonalConfigFile($file, $config);

        return true;
    }

    /**
     * @name Step3Form $form
     * @return bool
     * @throws ServerErrorHttpException
     */
    private function createAdminConfig(Step3Form $form): bool
    {
        $file = STD_FILENAME_ADMIN_PERSONAL_CONFIG;
        $config = ";Generated
            username = $form->username
            password = $form->password
            email = $form->email";

        $configHandler = new ConfigHandler();
        $configHandler->createPersonalConfigFile($file, $config);

        return true;
    }

    /**
     * @throws ServerErrorHttpException
     */
    private function createUploadsDir()
    {
        $uploadsDir = SW::getAlias('@uploads');
        $uploadsDirExists = file_exists($uploadsDir);
        if ($uploadsDirExists && !is_writable($uploadsDir)) {
            throw new ServerErrorHttpException("Please check write permissions for `$uploadsDir`");
        }
        if (!$uploadsDirExists && !mkdir($uploadsDir, 0777)) {
            throw new ServerErrorHttpException("Cannot create `$uploadsDir`. Check write permissions.");
        }
    }

    /**
     * @name Step3Form $form
     * @return bool
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    private function insertSettings(Step3Form $form): bool
    {
        $passwordSalt = SW::$app->security->generateRandomString();

        SettingModel::setEx('password_salt', $passwordSalt, 'Password salt', SettingModel::VISIBLE_NONE);
        SettingModel::setEx('admin_email', $form->email, 'Admin E-mail', SettingModel::VISIBLE_ROOT);
        SettingModel::setEx('robot_email', $form->robotEmail, 'Robot E-mail', SettingModel::VISIBLE_ROOT);
        SettingModel::setEx('recaptcha_key', $form->recaptchaKey, 'ReCaptcha key', SettingModel::VISIBLE_ROOT);
        SettingModel::setEx('recaptcha_secret', $form->recaptchaSecret, 'ReCaptcha secret', SettingModel::VISIBLE_ROOT);

        SettingModel::setEx('auth_time', 86400, 'Auth time', SettingModel::VISIBLE_ROOT);

        SettingModel::setEx('toolbar_position', 'top', 'Frontend toolbar position ("top", "bottom", "none")',
            SettingModel::VISIBLE_ROOT);

        return true;
    }

    /**
     * Выполнять создание пользователя можно только после того, как будут записанны настройки в базу
     * Это необходимо для правильной генерации хэша от пароля, т.к. используется настройка 'password_salt'
     * @name Step3Form $form
     * @return bool
     */
    private function insertUser(Step3Form $form): bool
    {
        $user = new UserModel();
        $user->username = $form->username;
        $user->password = $form->password;
        return $user->save();
    }

    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    private function installModules()
    {
        foreach (glob(SW::getAlias('@engine') . DIRECTORY_SEPARATOR . 'Modules/*') as $module) {
            $moduleName = basename($module);
            $className = "Steady\Admin\Modules\\$moduleName\\$moduleName" . 'Module';
            $class = $className;

            if (!class_exists($className) || !is_subclass_of($class, SteadyModule::class)) continue;

            /** @var SteadyModule $class */
            $moduleConfig = $class::$installConfig;

            //print_r($moduleConfig); exit();

            ModuleModel::install($moduleConfig);
        }
    }
}