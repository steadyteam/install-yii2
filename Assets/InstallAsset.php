<?php

namespace Steady\Install\Assets;

use yii\web\AssetBundle;

class InstallAsset extends AssetBundle
{
    public $sourcePath = '@install-frontend';
    public $css = [
        'css/install.css',
    ];
    public $js = [
        'js/install.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
