<?php

use Steady\Engine\Modules\Page\Models\PageModel;

class m000000_000060_add_route_column_to_pages_table extends \Steady\Engine\Base\Migration
{
    public function up()
    {
        $this->addColumnEx(PageModel::tableName(), 'route', $this->string(128));
    }

    public function safeDown()
    {
        $this->dropColumn(PageModel::tableName(), 'route');
    }
}
