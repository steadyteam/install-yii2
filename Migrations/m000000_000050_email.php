<?php

use Steady\Modules\Email\Models\EmailModel;

class m000000_000050_email extends \Steady\Engine\Base\Migration
{
    public function up()
    {
        EmailModel::migrationUp($this);
    }

    public function safeDown()
    {
        EmailModel::migrationDown($this);
    }
}
