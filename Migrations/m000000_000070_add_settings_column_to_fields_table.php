<?php

use Steady\Engine\Modules\Field\Models\FieldModel;
use Steady\Engine\Modules\Page\Models\PageModel;

class m000000_000070_add_settings_column_to_fields_table extends \Steady\Engine\Base\Migration
{
    public function up()
    {
        $this->addColumnEx(FieldModel::tableName(), 'settings', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn(FieldModel::tableName(), 'settings');
    }
}
