<?php

use Steady\Modules\Comment\Models\CommentAssignModel;
use Steady\Modules\Comment\Models\CommentModel;

class m000000_000030_comment extends \Steady\Engine\Base\Migration
{
    public function up()
    {
        CommentModel::migrationUp($this);
        CommentAssignModel::migrationUp($this);
    }

    public function safeDown()
    {
        CommentModel::migrationDown($this);
        CommentAssignModel::migrationDown($this);
    }
}
