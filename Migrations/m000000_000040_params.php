<?php

class m000000_000040_params extends \Steady\Engine\Base\Migration
{
    public function up()
    {
        foreach ($this->getTables() as $table) {
            $this->addColumnEx($table, 'params', $this->text());
        }
    }

    public function safeDown()
    {
        foreach ($this->getTables() as $table) {
            $this->dropColumn($table, 'params');
        }
    }

    /**
     * @return array
     */
    protected function getTables()
    {
        return [
            \Steady\Engine\Modules\Module\Models\ModuleModel::tableName(),
            \Steady\Engine\Modules\Setting\Models\SettingModel::tableName(),
            \Steady\Engine\Modules\User\Models\UserModel::tableName(),
            \Steady\Engine\Modules\Layout\Models\LayoutModel::tableName(),
            \Steady\Engine\Modules\Page\Models\PageModel::tableName(),
            \Steady\Engine\Modules\Block\Models\BlockModel::tableName(),
            \Steady\Engine\Modules\Text\Models\TextModel::tableName(),
            \Steady\Engine\Modules\Field\Models\FieldModel::tableName(),
            \Steady\Engine\Modules\Field\Models\FieldAssignModel::tableName(),
            \Steady\Engine\Models\PhotoModel::tableName(),
            \Steady\Engine\Models\SeoTextModel::tableName(),
            \Steady\Engine\Models\TagModel::tableName(),
            \Steady\Engine\Models\TagAssignModel::tableName(),
            \Steady\Modules\Catalog\Models\BrandModel::tableName(),
            \Steady\Modules\Catalog\Models\CategoryModel::tableName(),
            \Steady\Modules\Catalog\Models\GoodsModel::tableName(),
            \Steady\Modules\Shop\Models\CartModel::tableName(),
            \Steady\Modules\Shop\Models\CartAssignModel::tableName(),
            \Steady\Modules\Shop\Models\OrderModel::tableName(),
            \Steady\Modules\Shop\Models\OrderAssignModel::tableName(),
            \Steady\Modules\Comment\Models\CommentModel::tableName(),
            \Steady\Modules\Comment\Models\CommentAssignModel::tableName(),
            \Steady\Modules\Gallery\Models\GalleryModel::tableName(),
        ];
    }
}
