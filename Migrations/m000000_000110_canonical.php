<?php

use Steady\Engine\Models\SeoTextModel;

class m000000_000110_canonical extends \Steady\Engine\Base\Migration
{
    public function up()
    {
        $this->addColumn(SeoTextModel::tableName(), 'canonical', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn(SeoTextModel::tableName(), 'canonical');
    }
}
