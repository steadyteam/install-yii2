<?php

use Steady\Engine\Modules\Layout\Models\LayoutModel;

class m000000_000090_add_route_column_to_layouts_table extends \Steady\Engine\Base\Migration
{
    public function up()
    {
        $this->addColumnEx(LayoutModel::tableName(), 'route', $this->string(128));
    }

    public function safeDown()
    {
        $this->dropColumn(LayoutModel::tableName(), 'route');
    }
}
