<?php

use Steady\Engine\Modules\Dictionary\Models\DictionaryModel;

class m000000_000120_dictionary extends \Steady\Engine\Base\Migration
{
    public function up()
    {
        DictionaryModel::migrationUp($this);
    }

    public function safeDown()
    {
        DictionaryModel::migrationDown($this);
    }
}
