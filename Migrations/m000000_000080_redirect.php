<?php

use Steady\Modules\Seo\Models\RedirectModel;

class m000000_000080_redirect extends \Steady\Engine\Base\Migration
{
    public function up()
    {
        RedirectModel::migrationUp($this);
    }

    public function safeDown()
    {
        RedirectModel::migrationDown($this);
    }
}
