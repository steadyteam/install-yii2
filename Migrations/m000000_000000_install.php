<?php

use Steady\Engine\Models\PhotoModel;
use Steady\Engine\Models\SeoTextModel;
use Steady\Engine\Models\TagAssignModel;
use Steady\Engine\Models\TagModel;
use Steady\Engine\Modules\Block\Models\BlockModel;
use Steady\Engine\Modules\Field\Models\FieldAssignModel;
use Steady\Engine\Modules\Field\Models\FieldModel;
use Steady\Engine\Modules\Layout\Models\LayoutModel;
use Steady\Engine\Modules\Module\Models\ModuleModel;
use Steady\Engine\Modules\Page\Models\PageModel;
use Steady\Engine\Modules\Setting\Models\SettingModel;
use Steady\Engine\Modules\Text\Models\TextModel;
use Steady\Engine\Modules\User\Models\AuthModel;
use Steady\Engine\Modules\User\Models\UserModel;

class m000000_000000_install extends \Steady\Engine\Base\Migration
{
    const VERSION = 0.7;

    public $engine = 'ENGINE=MyISAM DEFAULT CHARSET=utf8';

    /**
     * @return bool|void
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        SettingModel::migrationUp($this);

        UserModel::migrationUp($this);
        AuthModel::migrationUp($this);

        ModuleModel::migrationUp($this);

        SeoTextModel::migrationUp($this);

        TagModel::migrationUp($this);
        TagAssignModel::migrationUp($this);

        PhotoModel::migrationUp($this);

        FieldModel::migrationUp($this);
        FieldAssignModel::migrationUp($this);

        LayoutModel::migrationUp($this);

        PageModel::migrationUp($this);

        BlockModel::migrationUp($this);

        TextModel::migrationUp($this);
    }

    public function safeDown()
    {
        SettingModel::migrationDown($this);
        UserModel::migrationDown($this);
        AuthModel::migrationDown($this);
        ModuleModel::migrationDown($this);

        SeoTextModel::migrationDown($this);
        TagModel::migrationDown($this);
        TagAssignModel::migrationDown($this);
        PhotoModel::migrationDown($this);

        FieldModel::migrationDown($this);
        FieldAssignModel::migrationDown($this);

        LayoutModel::migrationDown($this);
        PageModel::migrationDown($this);
        BlockModel::migrationDown($this);
        TextModel::migrationDown($this);
    }
}
