<?php

use Steady\Modules\Catalog\Models\AttributeModel;
use Steady\Modules\Catalog\Models\AttributesAssignModel;
use Steady\Modules\Catalog\Models\FilterModel;
use Steady\Modules\Catalog\Models\OfferModel;

class m000000_000100_offers_and_attributes extends \Steady\Engine\Base\Migration
{
    public function up()
    {
        OfferModel::migrationUp($this);
        AttributeModel::migrationUp($this);
        AttributesAssignModel::migrationUp($this);
        FilterModel::migrationUp($this);
    }

    public function safeDown()
    {
        OfferModel::migrationDown($this);
        AttributeModel::migrationDown($this);
        AttributesAssignModel::migrationDown($this);
        FilterModel::migrationUp($this);
    }
}
