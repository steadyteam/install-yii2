<?php

use Steady\Admin\Modules\System\SystemModule;
use Steady\Engine\Modules\Module\Models\ModuleModel;
use Steady\Modules\Catalog\CatalogModule;
use Steady\Modules\Catalog\Models\BrandModel;
use Steady\Modules\Catalog\Models\CategoryModel;
use Steady\Modules\Catalog\Models\GoodsModel;
use Steady\Modules\Shop\Models\CartAssignModel;
use Steady\Modules\Shop\Models\CartModel;
use Steady\Modules\Shop\Models\OrderAssignModel;
use Steady\Modules\Shop\Models\OrderModel;
use Steady\Modules\Shop\ShopModule;
use yii\db\StaleObjectException;

class m000000_000010_modules extends \Steady\Engine\Base\Migration
{
    /**
     * @return bool|void
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function up()
    {
        ModuleModel::install(CatalogModule::$installConfig);
        CategoryModel::migrationUp($this);
        BrandModel::migrationUp($this);
        GoodsModel::migrationUp($this);

        ModuleModel::install(ShopModule::$installConfig);
        CartModel::migrationUp($this);
        CartAssignModel::migrationUp($this);
        OrderModel::migrationUp($this);
        OrderAssignModel::migrationUp($this);
    }

    /**
     * @return bool|void
     * @throws Exception
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function safeDown()
    {
        ModuleModel::uninstall(CatalogModule::$installConfig['name']);
        CategoryModel::migrationDown($this);
        BrandModel::migrationDown($this);
        GoodsModel::migrationDown($this);

        ModuleModel::uninstall(ShopModule::$installConfig['name']);
        CartModel::migrationDown($this);
        CartAssignModel::migrationDown($this);
        OrderModel::migrationDown($this);
        OrderAssignModel::migrationDown($this);
    }
}
