<?php

use Steady\Engine\Modules\Module\Models\ModuleModel;
use Steady\Modules\Shop\Models\CartAssignModel;
use Steady\Modules\Shop\Models\CartModel;
use Steady\Modules\Shop\Models\OrderAssignModel;
use Steady\Modules\Shop\Models\OrderModel;
use Steady\Modules\Shop\ShopModule;

class m000000_000020_shop extends \Steady\Engine\Base\Migration
{
    public function up()
    {
        if (!ModuleModel::findAll(['name' => ShopModule::$installConfig['name']])) {
            CartModel::migrationUp($this);
            CartAssignModel::migrationUp($this);
            OrderModel::migrationUp($this);
            OrderAssignModel::migrationUp($this);
        }
    }

    public function safeDown()
    {
        if (ModuleModel::findAll(['name' => ShopModule::$installConfig['name']])) {
            CartModel::migrationDown($this);
            CartAssignModel::migrationDown($this);
            OrderModel::migrationDown($this);
            OrderAssignModel::migrationDown($this);
        }
    }
}
