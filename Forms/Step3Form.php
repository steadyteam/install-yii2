<?php

namespace Steady\Install\Forms;

use Steady\Engine\Base\Form;
use Steady\Engine\SW;

class Step3Form extends Form
{
    const RETURN_URL_KEY = 'sw_install_password';
    const ROOT_PASSWORD_KEY = 'sw_install_success_return';

    public $username;
    public $password;
    public $email;
    public $robotEmail;
    public $recaptchaKey;
    public $recaptchaSecret;

    public function rules()
    {
        return [
            ['username', 'string', 'min' => 8, 'max' => 16],
            ['username', 'required'],
            ['password', 'string', 'min' => 12, 'max' => 24],
            ['password', 'required'],
            ['email', 'email'],
            ['email', 'required'],
            ['robotEmail', 'email'],
            ['recaptchaKey', 'string'],
            ['recaptchaSecret', 'string'],
            [['username', 'password', 'email', 'robotEmail', 'recaptchaKey', 'recaptchaSecret'], 'trim'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => SW::t('install', 'Admin username'),
            'password' => SW::t('install', 'Admin password'),
            'email' => SW::t('install', 'Admin E-mail'),
            'robotEmail' => SW::t('install', 'Robot E-mail'),
        ];
    }
}