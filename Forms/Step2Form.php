<?php

namespace Steady\Install\Forms;

use Steady\Engine\Base\Form;
use Steady\Engine\SW;

class Step2Form extends Form
{
    public $host;
    public $database;
    public $username;
    public $password;
    public $prefix;

    public function rules()
    {
        return [
            ['host', 'string'],
            ['host', 'required'],
            ['database', 'string'],
            ['database', 'required'],
            ['username', 'string'],
            ['username', 'required'],
            ['password', 'string'],
            ['prefix', 'string'],
            [['host', 'database', 'username', 'password', 'prefix'], 'trim'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'host' => SW::t('install', 'Host'),
            'database' => SW::t('install', 'Database name'),
            'username' => SW::t('install', 'Username'),
            'password' => SW::t('install', 'Password'),
            'prefix' => SW::t('install', 'Table prefix'),
        ];
    }
}