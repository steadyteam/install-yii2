<?php

namespace Steady\Install\Controllers;

use Steady\Engine\Base\Controller;
use Steady\Engine\SW;
use Steady\Install\Forms\Step2Form;
use Steady\Install\Forms\Step3Form;
use Steady\Install\Services\InstallService;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class StepController extends Controller
{
    /**
     * @var string
     */
    public $layout = 'step';

    /**
     * @var string
     */
    public $defaultAction = 'step1';

    /**
     * @var InstallService
     */
    private $service;

    /**
     * @var bool
     */
    private $dbConnected = false;

    /**
     * @var bool
     */
    private $adminInstalled = false;

    /**
     * @inheritdoc
     */
    public function beforeAction($action): bool
    {
        $this->service = new InstallService();
        return parent::beforeAction($action);
    }

    /**
     * @return string|Response
     */
    public function actionStep1()
    {
        if ($this->adminInstalled) {
            return $this->redirect($this->dbConnected ? ['/'] : ['/install/step/2']);
        }
        return $this->render('step1');
    }

    /**
     * @return array|string|Response
     * @throws ServerErrorHttpException
     */
    public function actionStep2()
    {
        $form = new Step2Form();

        if ($form->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                return $this->ajaxValidate($form);
            } else {
                $result = $this->service->step2($form);
                if ($result) {
                    return $this->redirect(['/install/step/3']);
                }
                return $this->redirect(['/install/step/2']);
            }
        } else {
            return $this->render('step2', [
                'model' => $form,
            ]);
        }
    }

    /**
     * @return array|string|Response
     * @throws ServerErrorHttpException
     * @throws \Exception
     * @throws \yii\db\Exception
     */
    public function actionStep3()
    {
        $form = new Step3Form();

        if ($form->load(SW::$app->request->post())) {
            if (SW::$app->request->isAjax) {
                return $this->ajaxValidate($form);
            } else {
                $result = $this->service->step3($form);
                if ($result) {
                    return $this->redirect(['/install/step/5']);
                }
                return $this->redirect(['/install/step/3']);
            }
        } else {
            $form->robotEmail = 'noreply@' . SW::$app->request->serverName;
            //SW::$app->session->setFlash(Step3Form::RETURN_URL_KEY, '/setup/step/3');

            return $this->render('step3', [
                'model' => $form,
            ]);
        }
    }


    /**
     * @return string
     */
    public function actionStep5()
    {
        $result = [];
        $result[] = 'Module installed';
        return $this->render('step5', ['result' => $result]);
    }

    /**
     * @return string|Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionFinish()
    {
        $root_password = SW::$app->session->getFlash(Step3Form::ROOT_PASSWORD_KEY, true);
        $returnRoute = SW::$app->session->getFlash(Step3Form::RETURN_URL_KEY, '/admin');

        if ($root_password) {
            $loginForm = new LoginFormModel([
                'username' => 'root',
                'password' => $root_password,
            ]);
            if ($loginForm->login()) {
                return $this->redirect([$returnRoute]);
            }
        }

        return $this->render('finish');
    }

    /**
     * @name $text
     * @return string
     */
    private function showError($text)
    {
        return $this->render('error', ['error' => $text]);
    }
}
